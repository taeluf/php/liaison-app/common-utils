# Common Liaison Utilities
These are not intended to be robust and work for all use cases. These are utilities that I've made for myself that I use across different websites of my own.

Please use them. They might be helpful for you. But ... you might run into issues & there probably won't be that much documentation.


## Versioning
The version of this repo is not directly tied to the version of the Liaison repo. Sorry.


Just look at the Common.php file


## Dependencies
I have code scrawl listed as a dev dependency, but I'm not using it yet.


I have commonmark & my phtml package as suggestions rather than requirements ... in case you only want some of the functionality of this lib. 

There are no runtime checks for available packages. That's on you.
